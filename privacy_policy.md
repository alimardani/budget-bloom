# Privacy Policy for Standalone Budget Tracking Application

Thank you for using Budget Bloom application. We understand that your privacy is important, and we are committed to protecting your personal information. This Privacy Policy outlines how we collect, use, and disclose information in our standalone budget tracking application.

## Information We Collect

Our application does not require you to create an account or provide any personal information. We do not collect any data from your device or track your usage behavior. You can use our application also without having any access to the internet.

## Usage of Information

We do not collect, use, or share any information from our users, as we do not store any data in any database or connect to any server.

## Disclosure of Information

Since we do not collect, use, or store any personal information, we do not share your data with any third parties.

## Data Security

We take data security seriously and use reasonable measures to protect your information from unauthorized access, disclosure, alteration, or destruction. Our application does not connect to any server, and all data is stored locally on your device.

## Changes to Our Privacy Policy

We reserve the right to modify or update this Privacy Policy from time to time, so please review it periodically. Your continued use of the application after any changes to the Privacy Policy will signify your acceptance of the revised terms.

## Contact Us

If you have any questions or concerns about our Privacy Policy, please contact us at yaser.alimardany@gmail.com.

Thank you for using our Budget Bloom, and we hope that it helps you achieve your financial goals.